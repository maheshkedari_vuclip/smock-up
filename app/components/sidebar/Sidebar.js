import React, { Component } from 'react';
import { Nav, NavLink, NavItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import { spawn } from 'child_process';
import SidebarBase from './SidebarBase';
import styles from './Sidebar.css';

type Props = {};

export default class Sidebar extends Component<Props> {
  constructor(props) {
    super(props);
    this.props = { ...props, activeIndex: 1 };
  }

  startServer = () => {
    console.log('Attempting to start server');
    const childProcess = spawn('java', [
      '-jar',
      'wiremock-standalone.jar',
      '--port=8090'
    ]);
    childProcess.stderr.on('error', () => {
      console.log('Error has occurred while executing command');
    });
  };

  render() {
    return (
      <nav className={styles.sidenav}>
        <div className={styles.logo} />
        <Nav>
          <NavItem>
            <NavLink tag={Link} to="/server">
              Add New Server
            </NavLink>
          </NavItem>
        </Nav>
        <SidebarBase activeIndex="1" />
      </nav>
    );
  }
}
