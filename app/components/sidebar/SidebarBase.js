import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Accordion, Icon, AccordionContent } from 'semantic-ui-react';
import data from '../../resources/data/servers.json';

/**
 * Get mappings
 */
const getMappings = mappings =>
  mappings.map((mapping, index) => {
    const keyIndex = `service-${index}`;
    return (
      <div key={keyIndex}>
        <Link to="/service">{mapping.servicename}</Link>
      </div>
    );
  });

/**
 * Get Content
 */
const getContent = mappings => (
  <AccordionContent>{getMappings(mappings)}</AccordionContent>
);

const getTitle = title => (
  <Accordion.Title>
    <Link to="/server">{title}</Link>
    <Icon className="float-right" name="play circle outline" />
  </Accordion.Title>
);
/**
 * Map servers
 */
const servers = data.servers.map((server, index) => {
  const serverkey = server.servername + index;
  return {
    key: serverkey,
    title: getTitle(server.servername),
    content: getContent(server.mappings, index)
  };
});

type Props = {
  activeIndex: ?number
};

export default class SidebarBase extends Component<Props> {
  showServerDetails = () => {
    console.log('Expanded');
  };

  render() {
    const { activeIndex } = this.props;
    console.log('Active Index is ');
    console.log(activeIndex);

    return (
      <div>
        <Accordion
          inverted
          defaultActiveIndex={0}
          panels={servers}
          onTitleClick={this.showServerDetails}
        />
      </div>
    );
  }
}
