import React, { Component, Fragment } from 'react';

type Props = {};

export default class Base extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  render() {
    return (
      <Fragment>
        <div> Base Fragment Here </div>
      </Fragment>
    );
  }
}
