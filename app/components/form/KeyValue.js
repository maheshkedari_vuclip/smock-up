import React, { Component } from 'react';
import { Col, Row, FormGroup, Label, Input } from 'reactstrap';
import { Button } from 'semantic-ui-react';

type RowProps = {
  propkey: string,
  propvalue: string
};

const RowObj = (props: RowProps) => {
  const { propkey, propvalue } = props;
  return (
    <Row>
      <Col md={4}>
        <Input type="text" name={propkey} id={propkey} placeholder="Key" />
      </Col>
      <Col md={4}>
        <Input
          type="text"
          name={propvalue}
          id={propvalue}
          placeholder="Value"
        />
      </Col>
      <Col md={1}>
        <Button>-</Button>
      </Col>
    </Row>
  );
};

export default class KeyValue extends Component<Props> {
  props: Props;

  constructor(props: Props) {
    super(props);
    this.props = props;
  }

  render() {
    const { id, title, data } = this.props;
    console.dir(data);
    const rowRender = data.map((obj, i) => {
      const rowKey = `${i}_${id}_key`;
      return <RowObj key={rowKey} propkey={obj.key} propvalue={obj.value} />;
    });

    return (
      <FormGroup row>
        <Label for={id} md={3}>
          {title}
        </Label>
        <Col md={9}>{rowRender}</Col>
      </FormGroup>
    );
  }
}
