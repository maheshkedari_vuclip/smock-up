import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';

type Props = {
  id: string,
  title: string
};

const CheckboxStd = (props: Props) => {
  const { id, title } = props;
  return (
    <FormGroup check>
      <Input id={id} type="checkbox" /> <Label check>{title}</Label>
    </FormGroup>
  );
};

export default CheckboxStd;
