import React from 'react';
import { FormGroup, Col, Label, Input } from 'reactstrap';

type Props = {
  id: string,
  title: string,
  options: OptionProps[],
  selected: string | undefined
};

type OptionProps = {
  key: string | undefined,
  value: string,
  selected: any
};

const Option = (props: OptionProps) => {
  const { key, value, selected } = props;
  console.log(selected);
  return (
    <option value={key} selected={selected}>
      {value}
    </option>
  );
};

const ComboboxStd = (props: Props) => {
  const { id, title, options, selected } = props;
  const { key, value } = options[0];
  console.dir(typeof key);
  console.dir(value);
  const optionsDisplay = options.map(option => (
    <Option
      key={option.key}
      value={option.value}
      selected={option.key === selected}
    />
  ));
  return (
    <FormGroup row>
      <Label for={id} md={3}>
        {title}
      </Label>
      <Col md={9}>
        <Input type="select" name="select" id={id}>
          {optionsDisplay}
        </Input>
      </Col>
    </FormGroup>
  );
};

export default ComboboxStd;
