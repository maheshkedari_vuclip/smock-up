import React from 'react';
import { Col, FormGroup, Label, Input } from 'reactstrap';

type Props = {
  id: string,
  title: string,
  placeholder: string
};

const NumberField = (props: Props) => {
  const { id, title, placeholder } = props;
  return (
    <FormGroup row>
      <Label for={id} md={3}>
        {title}
      </Label>
      <Col md={9}>
        <Input
          type="number"
          name={id}
          id={id}
          placeholder={placeholder || ''}
        />
      </Col>
    </FormGroup>
  );
};

export default NumberField;
