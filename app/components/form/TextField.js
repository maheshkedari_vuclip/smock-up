import React from 'react';
import { Col, FormGroup, Label, Input } from 'reactstrap';

type Props = {
  id: string,
  title: string,
  placeholder: string,
  value: string
};

const TextFieldStd = (props: Props) => {
  const { id, title, placeholder, value } = props;
  return (
    <FormGroup row>
      <Label for={id} md={3}>
        {title}
      </Label>
      <Col md={9}>
        <Input
          name={id}
          id={id}
          placeholder={placeholder || ''}
          value={value}
        />
      </Col>
    </FormGroup>
  );
};

export default TextFieldStd;
