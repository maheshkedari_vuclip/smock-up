// @flow
import type { GetState, Dispatch } from '../reducers/ServiceType';

export const ADD_SERVICE = 'ADD_SERVICE';
export const ADD_SERVICE_SUCCESS = 'ADD_SERVICE_SUCCESS';
export const ADD_SERVICE_ERROR = 'ADD_SERVICE_ERROR';

export const ADD_REQUEST_HEADER = 'ADD_REQUEST_HEADER';
export const REMOVE_REQUEST_HEADER = 'REMOVE_REQUEST_HEADER';

export const ADD_RESPONSE_HEADER = 'ADD_RESPONE_HEADER';
export const REMOVE_RESPONSE_HEADER = 'REMOVE_RESPONSE_HEADER';

export const SAVE_SERVICE = 'SAVE_SERVICE';
export const SAVE_SERVICE_SUCCESS = 'SAVE_SERVICE_SUCCESS';
export const SAVE_SERVICE_ERROR = 'SAVE_SERVICE_ERROR';

export const CLEAR_SERVICE_FORM = 'CLEAR_SERVICE';

export function addService() {
  return {
    type: ADD_SERVICE
  };
}

export function addRequestHeader() {
  return {
    type: ADD_REQUEST_HEADER
  };
}

export function removeRequestHeader() {
  return {
    type: REMOVE_REQUEST_HEADER
  };
}

export function addResponseHeader() {
  return {
    type: ADD_RESPONSE_HEADER
  };
}

export function removeResponseHeader() {
  return {
    type: REMOVE_RESPONSE_HEADER
  };
}

function performSave() {
  console.log('Saving Service');
  return {
    type: SAVE_SERVICE_SUCCESS
  };
}

export function saveService() {
  return (dispatch: Dispatch, getState: GetState) => {
    const { counter } = getState();
    if (counter % 2 === 0) {
      return;
    }
    dispatch(performSave());
  };
}

export function clearService() {
  return {
    type: CLEAR_SERVICE_FORM
  };
}
/*
export function incrementIfOdd() {
  return (dispatch: Dispatch, getState: GetState) => {
    const { counter } = getState();
    if (counter % 2 === 0) {
      return;
    }
    dispatch(addService());
  };
}

export function incrementAsync(delay: number = 1000) {
  return (dispatch: Dispatch) => {
    setTimeout(() => {
      dispatch(addRequestHeader());
    }, delay);
  };
}
*/
