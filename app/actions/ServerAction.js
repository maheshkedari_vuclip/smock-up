// @flow
/* import type { GetState, Dispatch } from '../reducers/types'; */

export const ADD_SERVER = 'ADD_SERVER';
export const START_SERVER = 'START_SERVER';
export const SERVER_STARTED = 'SERVER_STARTED';
export const STOP_SERVER = 'STOP_SERVER';
export const SAVE_SERVER = 'SAVE_SERVER';
export const CLEAR_SERVER_FORM = 'CLEAR_SERVER';

export function addServer() {
  return {
    type: ADD_SERVER
  };
}

export function startServer() {
  return (dispatch: Dispatch, getState: GetState) => {
    const { counter } = getState();

    if (counter % 2 === 0) {
      return;
    }

    dispatch(() => ({ type: SERVER_STARTED }));
  };
}

export function stopServer() {
  return (dispatch: Dispatch, getState: GetState) => {
    const { counter } = getState();
    if (counter % 2 === 0) {
      return;
    }
    dispatch(() => ({ type: STOP_SERVER }));
  };
}

function performSaveServer() {
  /* Write to File Here */

  return {
    type: SAVE_SERVER
  };
}

export function saveServer() {
  return (dispatch: Dispatch, getState: GetState) => {
    const { counter } = getState();
    if (counter % 2 === 0) {
      return;
    }
    dispatch(performSaveServer());
  };
}

export function clearServerForm() {
  return {
    type: CLEAR_SERVER_FORM
  };
}

/*
export function incrementIfOdd() {
  return (dispatch: Dispatch, getState: GetState) => {
    const { counter } = getState();

    if (counter % 2 === 0) {
      return;
    }

    dispatch(addServer());
  };
}

export function incrementAsync(delay: number = 1000) {
  return (dispatch: Dispatch) => {
    setTimeout(() => {
      dispatch(saveServer());
    }, delay);
  };
}
*/
