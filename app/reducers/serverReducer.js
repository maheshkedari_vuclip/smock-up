// @flow
import {
  ADD_SERVER,
  START_SERVER,
  STOP_SERVER,
  CLEAR_SERVER_FORM,
  SAVE_SERVER
} from '../actions/ServerAction';
import type { Action } from './types';

export default function counter(state: number = 0, action: Action) {
  switch (action.type) {
    case ADD_SERVER:
      return state + 1;
    case START_SERVER:
      return state + 1;
    case STOP_SERVER:
      return state + 1;
    case SAVE_SERVER:
      return state - 1;
    case CLEAR_SERVER_FORM:
      return state - 2;
    default:
      return state;
  }
}
