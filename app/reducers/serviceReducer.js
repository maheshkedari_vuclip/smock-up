// @flow
import {
  ADD_SERVICE,
  ADD_REQUEST_HEADER,
  ADD_RESPONSE_HEADER,
  SAVE_SERVICE,
  CLEAR_SERVICE_FORM
} from '../actions/ServiceAction';
import type { Action } from './types';

export default function counter(state: number = 0, action: Action) {
  switch (action.type) {
    case ADD_SERVICE:
      return state + 1;
    case ADD_REQUEST_HEADER:
      return state - 1;
    case ADD_RESPONSE_HEADER:
      return state - 1;
    case SAVE_SERVICE:
      return state - 1;
    case CLEAR_SERVICE_FORM:
      return state - 1;
    default:
      return state;
  }
}
