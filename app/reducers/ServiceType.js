import type { Dispatch as ReduxDispatch, Store as ReduxStore } from 'redux';

export type serviceStateType = {
  +counter: number
};

export type Action = {
  +type: string
};

export type GetState = () => serviceStateType;

export type Dispatch = ReduxDispatch<Action>;

export type Store = ReduxStore<GetState, Action>;
