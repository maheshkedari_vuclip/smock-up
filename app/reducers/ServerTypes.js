import type { Dispatch as ReduxDispatch, Store as ReduxStore } from 'redux';

export type serverStateType = {
  +counter: number
};

export type Action = {
  +type: string
};

export type GetState = () => serverStateType;

export type Dispatch = ReduxDispatch<Action>;

export type Store = ReduxStore<GetState, Action>;
