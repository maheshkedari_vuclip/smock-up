/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import { Switch, Route } from 'react-router';
import routes from './constants/routes.json';
import App from './containers/App';
import HomePage from './containers/HomePage';
import BasePage from './containers/base/BaseContainer';
import Sidebar from './components/sidebar/Sidebar';
import ServerPage from './containers/server/ServerContainer';
import ServicePage from './containers/service/ServiceContainer';

export default () => (
  <App>
    <div className="wrapper_container">
      <Sidebar />
      <div className="main-content">
        <Switch>
          <Route path={routes.HOME} component={HomePage} />
          <Route path={routes.SERVICE} component={ServicePage} />
          <Route path={routes.SERVER} component={ServerPage} />
          <Route path={routes.BASE} exact component={BasePage} />
        </Switch>
      </div>
    </div>
  </App>
);
