import React, { Component } from 'react';
import { Form, Button } from 'reactstrap';
import { TextFieldStd, CheckboxStd } from '../../components/form';

type Props = {
  startServer: () => void,
  stopServer: () => void,
  saveServer: () => void,
  clearServerForm: () => void,
  server: any
};

export default class ServerComponent extends Component<Props> {
  props: Props;

  render() {
    const {
      startServer,
      stopServer,
      saveServer,
      clearServerForm,
      server
    } = this.props;
    return (
      <Form>
        <TextFieldStd
          id="server-name"
          title="Name"
          placeholder="Server-name"
          value={server.name}
        />
        <TextFieldStd
          id="server-host"
          title="Host"
          placeholder="http://localhost"
          value={server.host}
        />
        <TextFieldStd
          id="port"
          title="Port"
          placeholder="8090"
          value={server.port}
        />
        <CheckboxStd
          id="authentication"
          title="Authentication Required"
          value={server.authentication}
        />

        <Button onClick={saveServer}>Save</Button>
        <Button onClick={startServer}>Start</Button>
        <Button onClick={stopServer}>Stop</Button>
        <Button onClick={clearServerForm}>Clear</Button>
      </Form>
    );
  }
}
