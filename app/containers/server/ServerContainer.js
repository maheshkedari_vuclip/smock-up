import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ServerComponent from './ServerComponent';
import * as ServerActions from '../../actions/ServerAction';

function mapStateToProps(state) {
  return {
    server: {
      ...{
        name: '',
        host: '',
        port: '',
        authentication: true
      },
      ...state.selectedServer
    }
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ServerActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ServerComponent);
