import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ServiceComponent from './ServiceComponent';
import * as ServiceActions from '../../actions/ServiceAction';

function mapStateToProps(state) {
  return {
    service: {
      ...{},
      ...state.service
    }
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ServiceActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ServiceComponent);
