import React, { Component } from 'react';
import { Form, Button } from 'reactstrap';
import responseCodes from '../../constants/response_codes.json';
import {
  TextAreaStd,
  ComboboxStd,
  KeyValue,
  NumberField
} from '../../components/form';
import requestType from './requestTypeComponent';

type Option = {
  key: string,
  value: string
};

type Props = {
  addRequestHeader: () => void,
  addResponseHeader: () => void,
  clearService: () => void,
  saveService: () => void,
  removeRequestHeader: () => void,
  removeResponseHeader: () => void,
  responseCode: number
};

export default class ServiceComponent extends Component<Props> {
  props: Props;

  options: Option[];

  requestHeaders: Option[];

  responseHeader: Option[];

  addRequestHeader = () => {
    this.requestHeaders.push({
      key: '',
      value: ''
    });
  };

  addResponseHeader = () => {
    this.requestHeaders.push({
      key: '',
      value: ''
    });
  };

  render() {
    const {
      addRequestHeader,
      removeRequestHeader,
      addResponseHeader,
      removeResponseHeader,
      clearService,
      saveService,
      responseCode
    } = this.props;
    this.options = responseCodes.values;
    this.requestHeaders = [
      {
        key: 'content-type',
        value: 'Application/header'
      }
    ];

    this.responseHeader = [
      {
        key: 'content-type',
        value: 'Application/header'
      }
    ];

    return (
      <Form>
        {requestType()}
        <ComboboxStd
          id="responseCode"
          title="Response Code"
          options={this.options}
          selected={responseCode}
        />

        <KeyValue
          id="responseHeader"
          title="Request Header"
          data={this.requestHeaders}
          removeHandler={removeRequestHeader}
        />
        <Button color="link" onClick={addRequestHeader}>
          Add Header{' '}
        </Button>

        <TextAreaStd
          id="requestBody"
          title="Request Body"
          placeholder="Enter valid JSON here"
        />

        <KeyValue
          id="requestHeader"
          title="Response Header"
          data={this.requestHeaders}
          removeHandler={removeResponseHeader}
        />
        <Button color="link" onClick={addResponseHeader}>
          Add Header{' '}
        </Button>

        <TextAreaStd
          id="responseBody"
          title="Response Body"
          placeholder="Enter valid JSON here"
        />
        <NumberField id="delay" title="Delay" placeholder="Enter Delay in ms" />
        <Button onClick={saveService}>Save</Button>
        <Button onClick={clearService}>Clear</Button>
      </Form>
    );
  }
}
