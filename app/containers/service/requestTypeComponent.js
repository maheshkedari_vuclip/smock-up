import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';

const requestType = () => (
  <FormGroup tag="fieldset">
    <legend>Request Type</legend>
    <Label check md={2}>
      <Input type="radio" name="getRequest" /> GET
    </Label>
    <Label check md={2}>
      <Input type="radio" name="postRequest" /> POST
    </Label>
    <Label check md={2}>
      <Input type="radio" name="putRequest" /> PUT
    </Label>
    <Label check md={2}>
      <Input type="radio" name="deleteRequest" /> DELETE
    </Label>
    <Label check md={2}>
      <Input type="radio" name="patchRequest" /> PATCH
    </Label>
  </FormGroup>
);

export default requestType;
