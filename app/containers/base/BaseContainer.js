import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Base from '../../components/base/Base';
import * as BaseActions from '../../actions/base';

function mapStateToProps(state) {
  return {
    base: state.base
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(BaseActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Base);
